# Overview

This repository implements a small proof of concept Rest API that
uses JWT bearer tokens to authorise a protected
dashboard resource. An AWS Cognito user pool issues access and refresh tokens.
These are separately stored within HTTP only cookies. 

Currently, it is suitable for development environments with future improvements 
highlighted within the _Current Limitations and Future Work_ section.


# Rest API Endpoints

The Rest API is hosted within a [FastAPI](https://fastapi.tiangolo.com/) server 
that exposes authorisation endpoints for:

* **/auth/login**: Redirect client to sign-in page of AWS Cognito user pool.
  Upon successful sign-in, AWS Cognito redirects to a configured backend
  endpoint to resolve a code challenge for receipt of access, identity
  and refresh tokens. Access and refresh tokens are stored within 
  separate HTTP only cookies. The server concludes by redirecting the
  client to a dashboard page.
  
* **/auth/logout**: The server issues a request to the AWS Cognito
  [logout](https://docs.aws.amazon.com/cognito/latest/developerguide/logout-endpoint.html)
  endpoint to sign-out the client session. Upon successful sign-out the
  server deletes the access token and refresh token cookies before
  redirecting the client to the home page.
  
* **/auth/me**: Retrieve Open ID Connect (OIDC) UserInfo profile details.

* **/auth/refresh**: Allow client to refresh expired access token using the
  refresh token stored in HTTP only cookie. The server issues a refresh
  token request to the AWS Cognito user pool
  [token](https://docs.aws.amazon.com/cognito/latest/developerguide/token-endpoint.html)
  endpoint. Subsequently, the server updates the HTTP only cookie with 
  the new access token and returns a HTTP 204 no content response.

The server renders the front end dashboard and home pages using Jinja2
templates and Bootstrap.

# OpenAPI Documentation

The Rest API implementation uses [FastAPI](https://fastapi.tiangolo.com/). This facilitates 
the provision of OpenAPI documentation via Python typing and decorators of endpoint
methods.

Upon running the local development server the OpenAPI documentation can be viewed by 
visiting http://localhost:8000/docs or http://localhost:8000/redoc.

# Dependencies

This project has the following additional dependencies:

* [authlib](https://docs.authlib.org/en/latest/): Supports:
  
  - JWT verification 
    
  - Issue request for tokens from AWS Cognito user pool.
  
  - Issue refresh access token requests to AWS Cognito user pool.
  
* [black](https://black.readthedocs.io/en/stable/index.html): Automates 
  PEP 8 compliant formatting of Python source code.
  
* [flake8](https://flake8.pycqa.org/en/latest/): Supports PEP8 linting.

* [isort](https://pypi.org/project/isort/): Automates sorting of Python 
  imports. Imports are sorted alphabetically, and automatically separated 
  into sections and by type.
  
* [poetry](https://python-poetry.org/docs/): Facilitates management of Python dependencies


# Why Was The Project Implemented?

At the time of writing, [authlib](https://docs.authlib.org/en/latest/) 
does not provide an implementation for parsing and verifying access tokens
stored within HTTP only cookies. The author implemented this functionality 
within the _CookieResourceProtector_ and _CognitoTokenValidator_ classes,
located within the _app.auth_ module.

Furthermore, the _CookieResourceProtector_ allows 
[FastAPI](https://fastapi.tiangolo.com/) protected endpoints to be decorated
with oauth2 scopes to enable authorisation with an access token stored 
within the cookie:

```python
@router.get(
    "/me",
    response_description="Response received from AWS Cognito **UserInfo** endpoint",
    response_model=UserInfoModel,
    response_model_exclude_unset=True,
    summary="Return AWS Cognito user profile details",
)
@require_auth("openid")
async def user_info(
    request: Request,
    token: OAuth2Token = Depends(get_token_dict),
    context: AppContext = Depends(get_app_context),
) -> UserInfoModel:

    user: UserInfo = await context.oauth2_context.provider.userinfo(token=token)
    return UserInfoModel(**user)
```

# Configuration

The project should be used with an AWS Cognito user pool to authenticate and
receive access and tokens. The following settings should be configured within a
_.env_ file in the project root folder. A sample has been provided within the 
_.env-sample_ file:

- **client_id**: Client ID configured in AWS Cognito user pool.

- **client_secret**: Client secret configured in AWS Cognito user pool.

- **domain**: Domain configured in AWS Cognito user pool.

- **logout_redirect_uri**: Server endpoint to redirect to after logout. This should 
  match the setting configured in AWS user pool.
  
- **region**: The region of the AWS user pool 
  
- **redirect_uri**: Server endpoint that resolves the authentication code challenge
  issued by the AWS Cognito user pool after successful sign-in.
  
- **userpool_id**: AWS cognito user pool ID.


# Current Limitations and Future Work

Unit and integration tests must be implemented in addition to a CI build environment.

Currently, cookies are HTTP only. However, they are only suitable for use in a
development environment. In production environments the cookies should also be 
used only with secure HTTP within an allocated domain.

The login and logout endpoints will be refactored to accept
a query parameter that represents a redirect url. Upon successful sign-in and 
sign-out the server will redirect to these client urls.

# Quickstart

If not already configure install poetry by following the instructions 
[here](https://python-poetry.org/docs/).

Ensure that an AWS Cognito user pool is available and create a _.env_ within 
the project root folder. A _.env-sample_ file has been provided and can be copied
and renamed to _.env_. Finally, configure the settings to match those of the
AWS Cognito user pool. Ensure that the redirect urls, client ID etc. match those
in the AWS Cognito user pool.

Within a command shell or terminal issue the following commands:

```bash
# create a virtual environment for the project
poetry shell

# install the dependencies
poetry install

# run the API server
python main.py
```

Further information on poetry usage is available 
[here](https://python-poetry.org/docs/basic-usage/).

Open a web browser and visit http://localhost:8000

View the OpenAPI documentation by visiting http://localhost:8000/docs or
http://localhost:8000/redoc.