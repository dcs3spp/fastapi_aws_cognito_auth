"""jwt module provides create_jwt_from_base64 to create Cognito JWT.

The module also defines the following errors associated with converting a
base64 to an in memory JWT representation:
- JWTDecodeError: Failed to decode and verify.
- JWTFormatError: JWT does not have a header, payload, signature etc.
- JWTInvalidSignatureError: JWT signature verification failed.
- JWTClaimValidationError: ud / client_id, exp or iss claims are invalid.
"""
import base64
import binascii
import json

import httpx
from authlib.jose import JsonWebKey, jwt
from authlib.jose.errors import (
    BadSignatureError,
    ExpiredTokenError,
    InvalidClaimError,
    MissingClaimError,
)
from authlib.jose.rfc7517 import KeySet
from authlib.oauth2.rfc7523.validator import JWTBearerToken


class JWTDecodeError(Exception):
    """JWTDecodeError is raised for failure to decode and verify a jwt."""

    pass


class JWTFormatError(Exception):
    """JwtFormatError is raised for when JWT has incorrect structure."""

    pass


class JWTInvalidSignatureError(Exception):
    """JWTInvalidSignatureError is raised when JWT signature fails verification."""

    pass


class JWTClaimValidationError(Exception):
    """JWTClaimValidationError is raised when aud / client_id, exp or iss claims
    are invalid."""

    pass


class AWSCognitoJwt(object):
    """Representation of an AWS Cognito JWT.

    An AWS Cognito JWT can be used as an access or identity token.
    At the time of writing the identity token contains the traditional (aud) claim.
    However, the access token equivalent is the (client_id) claim.
    """

    def __init__(self, header: dict, payload: dict, token_str: str):
        """Store the header, payload and originating base64 token string for
        an AWS Cognito jwt.

        :raise:
            JWTFormatError when missing 'token_use' attribute or it contains a value
            other than 'access' or 'id'.
        """
        token_use = payload.get("token_use")
        if token_use is None:
            raise JWTFormatError("JWT is missing 'token_use' attribute")

        options = {"exp": {"essential": True}, "iss": {"essential": True}}

        if token_use == "access":
            options["client_id"] = {"essential": True}
        elif token_use == "id":
            options["aud"] = {"essential": True}
        else:
            raise JWTFormatError(
                f"JWT has invalid 'token_use' attribute value : {token_use}"
            )

        self._keyset = None
        self._token = JWTBearerToken(payload, header, options)
        self._token_str = token_str
        self._token_use = token_use

    @property
    def token(self) -> JWTBearerToken:
        """Claims of jwt represented as a dict."""
        return self._token

    @property
    def token_str(self) -> str:
        """The original base64 token string."""
        return self._token_str

    @property
    def token_use(self) -> str:
        """Describes the intended jwt usage. Can be 'access' or 'id'."""
        return self._token_use

    async def verify(self, jwks_url: str, refresh_cache: bool = False) -> None:
        """An AWS jwt is trusted if the following verifications pass.

        - signature is verified against public key used to sign the token from jwks_url
        - token is not expired.
        - (aud) or (client_id) claim matches app client ID of user pool.
        - (iss) claim must be https://cognito-idp.<region>.amazonaws.com/<user pool Id>
        - (token_use) claim contains 'access' or 'id'.

        If the JWT passes verification then no exception is raised.

        :param:
            jwks_url is url to jwks cognito endpoint
        :param:
            jwks used for signing is cached. If this parameter is True
            then the cache is invalidated and the jwks is downloaded again.

        :raise:
            httpx.RequestError when client http failure occurs, e.g. timeout, while
            dowloading jwks.
        :raise
            httpx.HTTPStatusError when error response received while downloading jwks.
        :raise:
            JWTFormatError when 'kid' attribute is missing in header.
        :raise:
            JWTInvalidSignatureError when signature verification fails.
        :raise:
            JWTClaimValidationError when essential claim is mssing or token has exited.
        """

        if self._keyset is None or refresh_cache is True:
            self._keyset = await self._get_keyset(jwks_url)

        token_kid = self._token.header["kid"]

        try:
            key = self._keyset.find_by_kid(token_kid)
        except ValueError as ve:
            raise JWTFormatError("Missing kid header attribute") from ve

        pem = key.as_pem()

        try:
            claims = jwt.decode(self.token_str, pem)
        except BadSignatureError as bse:
            raise JWTInvalidSignatureError("Signature verification failed") from bse

        try:
            claims.validate()
        except InvalidClaimError as ice:
            raise JWTClaimValidationError("Invalid claim encountered") from ice
        except MissingClaimError as mce:
            raise JWTClaimValidationError("An essential claim was missing") from mce
        except ExpiredTokenError as ete:
            raise JWTClaimValidationError("Token expiration encountered") from ete

    async def _get_keyset(self, jwks_url: str) -> KeySet:
        """Download jwks keystore.

        :returns: authlib.jose.rfc7517.KeySet intialised from jwks endpoint.

        :raise:
            httpx.RequestError when client http failure occurs, e.g. timeout, while
            dowloading jwks.
        :raise
            httpx.HTTPStatusError when error response received while downloading jwks.
        """
        async with httpx.AsyncClient() as client:
            resp = await client.get(jwks_url)
            resp.raise_for_status()

            return JsonWebKey.import_key_set(resp.text)


def create_jwt_from_base64(jwt: str) -> AWSCognitoJwt:
    """
    Return a :class: AWSCognitoJwt instance from a base64 encoded
    jwt token.

    :param: base64 encoded jwt

    :raise:
        JWTDecodeError is raised when one of the following occur:
        - jwt does not contain header, payload and signature.
        - failed to decode utf-8 from byte representation of header
          payload or signature.
        - failed to decode base64 for header, payload or signature.

    :raise:
        JWTFormatError is raised when one of the following occur:
        - jwt does not contain header, payload or signature
        - jwt is missing token_use attribute
        - jwt contains a value other than 'access' or 'id' for
          'token_use' attribute.

    :return: :class:```AWSCognitoJwt```
    """
    parts = jwt.split(".")

    if len(parts) != 3:
        raise JWTFormatError("JWT should contain a header, payload and signature")

    try:
        hdr = base64.urlsafe_b64decode(parts[0] + "=" * (4 - len(parts[0]) % 4))
        payload = base64.urlsafe_b64decode(parts[1] + "=" * (4 - len(parts[1]) % 4))
        _ = base64.urlsafe_b64decode(parts[2] + "=" * (4 - len(parts[2]) % 4))
    except binascii.Error:
        raise JWTDecodeError("Failed to perform base64 decoding of jwt")

    try:
        hdr_dict = json.loads(hdr.decode("utf-8"))
        payload_dict = json.loads(payload.decode("utf-8"))
    except UnicodeError:
        raise JWTDecodeError("Failed to decode header or payload as utf-8")
    except json.JSONDecodeError:
        raise JWTDecodeError("Failed to decode json in header or payload segment")

    return AWSCognitoJwt(hdr_dict, payload_dict, jwt)
