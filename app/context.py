from .auth import OAuth2Context
from .cognito import AWSCognito


class AppContext:
    """AppContext class provides properties to access oauth2 context and settings"""

    def __init__(self) -> None:
        """Initialise context and settings to None"""
        self._oauth2_context = None
        self._oauth2_settings = None

    @property
    def oauth2_context(self) -> OAuth2Context:
        """Context providing access to AWS Cognito token validator and resource
        protection of resources by verifying access token stored in cookie

        :getter: Access oauth2 context
        :setter: Set the AWS oauth2 context
        :type: :class:`OAuth2Context`
        """
        return self._oauth2_context

    @oauth2_context.setter
    def oauth2_context(self, val: OAuth2Context) -> None:
        self._oauth2_context = val

    @property
    def oauth2_settings(self) -> AWSCognito:
        """Configuration settings for AWS user pool

        :getter: Access AWS Cognito user pool settings
        :setter: Set the AWS Cognito user pool settings
        :type: :class:`AWSCognito`
        """
        return self._oauth2_settings

    @oauth2_settings.setter
    def oauth2_settings(self, val: AWSCognito) -> None:
        self._oauth2_settings = val
