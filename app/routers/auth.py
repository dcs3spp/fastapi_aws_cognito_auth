from authlib.integrations.httpx_client import AsyncOAuth2Client
from authlib.jose.errors import BadSignatureError, DecodeError, InvalidClaimError
from authlib.oauth2.rfc6749 import OAuth2Token
from authlib.oidc.core.claims import UserInfo
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder
from fastapi.requests import Request
from fastapi.responses import JSONResponse, RedirectResponse

from ..context import AppContext
from ..dependencies import (
    get_access_token,
    get_app_context,
    get_refresh_token,
    get_token_dict,
    require_auth,
)
from ..doc import auth_responses
from ..models.auth import UserInfoModel

router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)


@router.get(
    "/aws_cognito_redirect",
    include_in_schema=False,
    response_class=RedirectResponse,
    response_description="Redirect to dashboard endpoint",
    status_code=status.HTTP_307_TEMPORARY_REDIRECT,
    summary="Store access and refresh tokens in cookies and redirect to dashboard",
)
async def read_code_challenge(
    request: Request,
    context: AppContext = Depends(get_app_context),
) -> RedirectResponse:

    """Request a token from AWS Cognito user pool using code challenge response and
    redirect to /dashboard.

    httponly cookies are returned containing access and refresh tokens.
    """

    token = await context.oauth2_context.provider.authorize_access_token(request)
    token_encoded = jsonable_encoder(token)

    response = RedirectResponse(url="/dashboard")
    response.set_cookie(
        httponly=True,
        key="session_token",
        secure=False,
        value="Bearer " + token_encoded["access_token"],
    )
    response.set_cookie(
        httponly=True,
        key="refresh_token",
        secure=False,
        value=token_encoded["refresh_token"],
    )

    return response


@router.get(
    "/login",
    response_class=RedirectResponse,
    response_description="Redirect to dashboard",
    status_code=status.HTTP_302_FOUND,
    summary="Login and redirect to the dashboard",
)
async def login(
    request: Request,
    context: AppContext = Depends(get_app_context),
) -> RedirectResponse:
    """Redirect to configured redirection url after sign-in."""
    redirect_uri = request.url_for("read_code_challenge")

    response: RedirectResponse = (
        await context.oauth2_context.provider.authorize_redirect(request, redirect_uri)
    )

    return response


@router.get(
    "/logout",
    response_class=RedirectResponse,
    responses={
        **auth_responses,
        status.HTTP_307_TEMPORARY_REDIRECT: {"description": "Redirect to home"},
    },
    status_code=status.HTTP_307_TEMPORARY_REDIRECT,
    summary="Logout and redirect to home",
)
@require_auth("openid")
async def logout(
    request: Request, context: AppContext = Depends(get_app_context)
) -> RedirectResponse:
    """Remove access and refresh token cookies and redirect to configured logout url."""

    response = RedirectResponse(url=context.oauth2_settings.auth_urls.logout_url)
    response.delete_cookie(key="session_token")
    response.delete_cookie(key="refresh_token")

    return response


@router.get(
    "/me",
    response_description="Response received from AWS Cognito **UserInfo** endpoint",
    response_model=UserInfoModel,
    response_model_exclude_unset=True,
    summary="Return AWS Cognito user profile details",
)
@require_auth("openid")
async def user_info(
    request: Request,
    token: OAuth2Token = Depends(get_token_dict),
    context: AppContext = Depends(get_app_context),
) -> UserInfoModel:

    user: UserInfo = await context.oauth2_context.provider.userinfo(token=token)
    return UserInfoModel(**user)


@router.get(
    "/refresh",
    response_class=JSONResponse,
    responses={
        **auth_responses,
        status.HTTP_204_NO_CONTENT: {
            "description": (
                "**session_token** cookie updated with refreshed access token"
            )
        },
    },
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Refresh access token and update in **session_token** cookie",
)
async def refresh(
    access_token: str = Depends(get_access_token),
    context: AppContext = Depends(get_app_context),
    refresh_token: str = Depends(get_refresh_token),
) -> JSONResponse:
    """
    Refreshes an access token and updates it's value in the **session_token** cookie.
    The access token is refreshed using the token stored in the **refresh_token**
    cookie.

    At the time of writing AWS Cognito does not rotate refresh tokens, after initial
    use. Consequently, a user's refresh token is used to refresh their corresponding
    access token each time it needs to be renewed due to expiry.
    \f
    :param access_token: Value stored in session_token cookie
    :type access_token: str

    :param context: AppContext that provides cognito configuration and token validators
    :type context: :class:`AppContext`

    :refresh_token: Value stored in refresh_token_cookie
    :type refresh_token: str

    :raises: :class:`HTTPException`:
        400 - session_token or refresh_token cookies are missing.
        401 - access token could not be decoded, has a bad signature, bad exp time value
                or has not yet expired.
    """

    try:
        is_expired = context.oauth2_context.token_validator.has_token_expired(
            access_token
        )
    except BadSignatureError as bse:
        raise HTTPException(
            status_code=401, detail=f"Bad JWT signature error := {bse.result}"
        )
    except DecodeError as de:
        raise HTTPException(
            status_code=401, detail=f"Decode JWT error := {de.description}"
        )
    except InvalidClaimError:
        raise HTTPException(status_code=401, detail="Invalid value for exp claim")

    if not is_expired:
        raise HTTPException(status_code=401, detail="JWT has not expired")

    async_client = AsyncOAuth2Client(
        context.oauth2_context.cognito_config.client_id,
        context.oauth2_context.cognito_config.client_secret,
    )

    token = await async_client.refresh_token(
        context.oauth2_context.cognito_config.auth_urls.refresh_url,
        refresh_token=refresh_token,
        headers={
            "Content-Type": "application/x-www-form-urlencoded",
        },
    )

    response = JSONResponse()
    response.delete_cookie("session_token")
    response.set_cookie(
        httponly=True,
        key="session_token",
        secure=False,
        value="Bearer " + token["access_token"],
    )
    response.status_code = status.HTTP_204_NO_CONTENT

    return response
