from fastapi import APIRouter, Depends
from fastapi.requests import Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from ..dependencies import get_templates, require_auth

router = APIRouter(
    tags=["core"],
)


@router.get("/", include_in_schema=False, summary="Render the home page")
async def home(request: Request, templates: Jinja2Templates = Depends(get_templates)):
    """Render home page"""
    return templates.TemplateResponse("index.html", {"request": request})


@router.get(
    "/dashboard",
    include_in_schema=False,
    response_class=HTMLResponse,
    summary="Render the dashboard",
)
@require_auth("openid")
async def dashboard(
    request: Request, templates: Jinja2Templates = Depends(get_templates)
):
    """Render dashboard area for authorized users."""
    return templates.TemplateResponse("dashboard.html", {"request": request, "id": id})
