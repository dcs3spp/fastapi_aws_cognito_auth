from typing import Optional

from pydantic import BaseModel


class UserInfoModel(BaseModel):
    """
    Class that represents the OpenID Connect (OIDC) claims from UserInfo endpoint
    response.

    The OpenID Connect (OIDC) specification explains that the sub claim is mandatory.
    The aud and iss claims should be present if the response from the UserInfo endpoint
    is signed.

    For further information visit the following links:
    https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims
    https://openid.net/specs/openid-connect-core-1_0.html#UserInfoResponse
    """

    address: Optional[str] = None
    aud: Optional[str] = None
    birthdate: Optional[str] = None
    email: Optional[str] = None
    email_verified: Optional[bool] = False
    family_name: Optional[str] = None
    gender: Optional[str] = None
    given_name: Optional[str] = None
    locale: Optional[str] = None
    middle_name: Optional[str] = None
    iss: Optional[str] = None
    name: Optional[str] = None
    nickname: Optional[str] = None
    phone_number: Optional[str] = None
    phone_number_verified: Optional[bool] = False
    preferred_username: Optional[str] = None
    picture: Optional[str] = None
    profile: Optional[str] = None
    sub: str
    updated_at: Optional[int] = None
    zoneinfo: Optional[str] = None
