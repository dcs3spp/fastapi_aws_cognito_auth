import datetime
import functools
import json
import logging
from datetime import timezone
from typing import Callable, Dict, Union

from authlib.integrations.starlette_client import OAuth
from authlib.integrations.starlette_client.apps import StarletteOAuth2App
from authlib.jose import JoseError, KeySet, jwt
from authlib.jose.errors import ExpiredTokenError
from authlib.jose.rfc7517 import Key
from authlib.oauth2 import OAuth2Error
from authlib.oauth2.rfc6749 import (
    HttpRequest,
    MissingAuthorizationError,
    OAuth2Token,
    ResourceProtector,
    TokenValidator,
    UnsupportedTokenTypeError,
)
from authlib.oauth2.rfc6750 import BearerTokenValidator
from authlib.oauth2.rfc7523.validator import JWTBearerToken
from fastapi import Request
from fastapi.exceptions import HTTPException

from . import cognito

logger = logging.getLogger(__name__)
Provider = StarletteOAuth2App


class CookieResourceProtector(ResourceProtector):
    def __init__(self, cookie_key: str = "session_token") -> None:
        """Initialise cookie key to parse."""

        super().__init__()
        self._cookie_key = cookie_key

    def parse_request_authorization(
        self, request: HttpRequest
    ) -> (TokenValidator, str):
        """Parse a request for HTTP only cookie containing JWT.

        Override parse_request_authorization from
        https://github.com/lepture/authlib/blob/169c7dcfc47478c8d55553cc95fb0f5578162b77/authlib/oauth2/rfc6749/resource_protector.py#L108

        :return: validator, token_string
        :raise: MissingAuthorizationError raised when the request is missing
            the cookie with expected key
        :raise: UnsupportedTokenTypeError is raised when the request contains
            the cookie with an invalid string
        """

        if self._cookie_key not in request.req.cookies:
            raise MissingAuthorizationError(
                self._default_auth_type, self._default_realm
            )

        cookie: str = request.req.cookies.get(self._cookie_key)

        token_parts = cookie.split(None, 1)
        if len(token_parts) != 2:
            raise UnsupportedTokenTypeError(
                self._default_auth_type, self._default_realm
            )

        token_type, token_string = token_parts
        validator = self.get_token_validator(token_type)

        return validator, token_string

    def raise_error_response(self, error):
        """Raise HTTPException for OAuth2Error. Developers can re-implement
        this method to customize the error response.
        :param error: OAuth2Error
        :raise: HTTPException
        """
        status = error.status_code
        body = json.dumps(dict(error.get_body()))
        headers = error.get_headers()

        raise HTTPException(
            detail=f"status: {status}\nbody: {body}\nheaders: {headers}\n",
            status_code=status,
        )

    def acquire_token(self, req: Request, scopes=None):
        """A method to acquire current valid token with the given scope.
        :param req: the request
        :param scopes: a list of scope values
        :return: token object
        """
        request = HttpRequest(req.method, req.url, req.state, req.headers)
        request.req = req

        if isinstance(scopes, str):
            scopes = [scopes]

        # validate request parses the request for the cookie
        token = self.validate_request(scopes, request)
        return token

    def __call__(self, scopes=None, optional=False):
        def wrapper(function):
            @functools.wraps(function)
            async def decorated(*args, **kwargs):

                matches = [x for x in kwargs.items() if isinstance(x[1], Request)]
                if len(matches) > 0:
                    request = matches[0][0]
                    try:
                        self.acquire_token(kwargs[request], scopes)
                    except MissingAuthorizationError as error:
                        if optional:
                            return function(*args, **kwargs)
                        self.raise_error_response(error)
                    except OAuth2Error as error:
                        self.raise_error_response(error)
                    return await function(*args, **kwargs)
                else:
                    raise ValueError("Missing request argument in router function")

            return decorated

        return wrapper


def get_public_key(key_set: KeySet) -> Callable[[dict, dict], Key]:
    """
    Return a function to load the key for the corresponding kid in a JWT header
    :param key_set: Authlib key set loaded from jwks endpoint
    :return: Function for loading the key in the JWT header, from the keyset
    """

    def get_key(header: dict, payload: dict) -> Key:
        kid = header.get("kid")
        if kid is None:
            raise ValueError("kid attribute missing form header")
        return key_set.find_by_kid(kid)

    return get_key


class CognitoTokenValidator(BearerTokenValidator):
    """
    Validate cognito identity / access token. Cognito does not provide a token
    introspection endpoint.

    A token is authenticated by:
    1. Download token signing keys from the JWKS endpoint
    2. Use a library to verify the token signature
    3. Verify (exp), (iss), (aud) and (token_use) claims

    """

    TOKEN_TYPE = "bearer"
    token_cls = JWTBearerToken

    def __init__(
        self,
        cognito_config: cognito.AWSCognito,
        func_get_public_key: Callable[[dict, dict], Key],
        usage="access",
        realm=None,
        **extra_attributes,
    ):
        super(CognitoTokenValidator, self).__init__(realm, **extra_attributes)

        self._cognito = cognito_config
        self._get_public_key = func_get_public_key

        claims_options = {
            "exp": {"essential": True},
            "iss": {"essential": True, "value": cognito_config.issuer},
            "token_use": {"essential": True},
        }

        if usage == "access":
            claims_options["client_id"] = {
                "essential": True,
                "value": cognito_config.client_id,
            }
        elif usage == "id":
            claims_options["aud"] = {
                "essential": True,
                "value": cognito_config.client_id,
            }
        else:
            raise ValueError("Invalid value for token_use. Expected 'access' or 'id'")

        self.claims_options = claims_options

    def authenticate_token(self, token_string):
        """Verify cognito jwt signature and (aud / client_id) (exp), (iss), (token_use)
        claims."""
        try:
            claims = jwt.decode(
                token_string,
                self._get_public_key,
                claims_options=self.claims_options,
                claims_cls=self.token_cls,
            )
            claims.validate()
            return claims
        except JoseError as error:
            logger.debug("Authenticate token failed. %r", error)
            return None

    def token_to_dict(self, access_token: str) -> OAuth2Token:
        """Decode an access token string and return authlib OAuth2 token.

        :param access_token: The access token to convert.
        :type access_token: str

        :raises: :class:`BadSignatureError`: Token has invalid signature
        :raises: :class:`DecodeError`: Token failed to be decoded

        :returns: authlib oauth2 token, containing access token, token type and expiry.
        :rtype: :class:`OAuth2Token`

        """
        claims = jwt.decode(
            access_token,
            self._get_public_key,
            claims_options=self.claims_options,
            claims_cls=self.token_cls,
        )

        token_dict: Dict[str, Union[str, str]] = {
            "access_token": access_token,
            "expires_at": claims["exp"],
            "token_type": "Bearer",
        }

        return OAuth2Token.from_dict(token_dict)

    def has_token_expired(self, token_string: str) -> bool:
        """Return True if the token has expired from current UTC unix time epoch.

        :param token_string: The token to validate for expiration.
        :type token_string: str

        :raises: :class:`BadSignatureError`: Token has invalid signature
        :raises: :class:`DecodeError`: Token failed to be decoded
        :raises: :class:`InvalidClaimError`: Invalid exp time is held in token

        :returns: True of token has expired, otherwise False
        :rtype: bool
        """
        claims = jwt.decode(token_string, self._get_public_key)

        dt = datetime.datetime.now(timezone.utc)
        utc_time = dt.replace(tzinfo=timezone.utc)

        try:
            claims.validate_exp(now=int(utc_time.timestamp()), leeway=0)
        except ExpiredTokenError:
            return True

        return False


class OAuth2Context(object):
    """Facade class for access to cognito authlib client, registry, token validator and
    resource protector."""

    def __init__(
        self,
        cognito_config: cognito.AWSCognito,
        provider,
        registry: OAuth,
        resource_protector: CookieResourceProtector,
        token_validator: CognitoTokenValidator,
    ):
        self._cognito_config = cognito_config
        self._provider = provider
        self._registry = registry
        self._resource_protector = resource_protector
        self._token_validator = token_validator

    @property
    def cognito_config(self) -> cognito.AWSCognito:
        return self._cognito_config

    @property
    def provider(self) -> Provider:
        return self._provider

    @property
    def registry(self) -> OAuth:
        return self._registry

    @property
    def resource_protector(self) -> CookieResourceProtector:
        return self._resource_protector

    @property
    def token_validator(self) -> CognitoTokenValidator:
        return self._token_validator


async def create_cognito_client(
    cognito_config: cognito.AWSCognito,
    resource_protector: CookieResourceProtector,
    usage: str = "access",
) -> OAuth2Context:
    """Factory method to create an OAuth2Provider instance from settings.
    Improve design by using builder pattern here."""

    keyset = await cognito_config.auth_urls.get_keyset()
    if keyset is None:
        raise ValueError("Failed to initialise JWKS")

    oauth = OAuth()
    provider = oauth.register(
        "cognito",
        client_id=cognito_config.client_id,
        client_secret=cognito_config.client_secret,
        server_metadata_url=cognito_config.auth_urls.discovery_url,
        client_kwargs={"scope": "openid email"},
    )

    validator = CognitoTokenValidator(
        cognito_config, get_public_key(keyset), usage, None
    )

    resource_protector.register_token_validator(validator)

    return OAuth2Context(cognito_config, provider, oauth, resource_protector, validator)
