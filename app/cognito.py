from __future__ import annotations

import logging

import httpx
from authlib.jose import JsonWebKey

from . import config

logger = logging.getLogger(__name__)


class AWSCognito(object):
    """
    Facade class that constructs AWS Cognito user pool config from application settings

    :param settings: The configuration settings used to construct AWS Cognito
        authorisation urls
    :type settings: :class:`config.Settings`
    """

    def __init__(self, settings: config.Settings):
        """Initialise oauth2 cognito configuration and urls.
        """
        self._client_id = settings.client_id
        self._client_secret = settings.client_secret
        self._domain = settings.domain
        self._redirect_uri = settings.redirect_uri
        self._region = settings.region
        self._userpool_id = settings.userpool_id

        self._base_url = f"https://{self._domain}.auth.{self._region}.amazoncognito.com"
        self._issuer = (
            f"https://cognito-idp.{self._region}.amazonaws.com/{self._userpool_id}"
        )
        self._jwks_url = f"{self._issuer}/.well-known/jwks.json"
        self._discovery_url = f"{self._issuer}/.well-known/openid-configuration"
        self._logout_url = (
            self._base_url
            + "/logout?client_id="
            + self._client_id
            + "&logout_uri="
            + settings.logout_redirect_uri
        )

        self._auth_urls = AWSCognitoAuthInfo(
            self._base_url,
            self._discovery_url,
            self._jwks_url,
            self._logout_url,
        )

    @property
    def auth_urls(self) -> AWSCognitoAuthInfo:
        """Access AWS Cognito authorisation urls.

        :getter: Returns accessor for authorisation urls
        :type: :class:`AWSCognitoAuthInfo`
        """
        return self._auth_urls

    @property
    def client_id(self) -> str:
        """The client id configured for AWS Cognito

        :getter: Returns the client id
        :type: str
        """
        return self._client_id

    @property
    def client_secret(self) -> str:
        """The client secret configured for AWS Cognito

        :getter: Returns the client secret
        :type: str
        """
        return self._client_secret

    @property
    def domain(self) -> str:
        """The domain configured for AWS Cognito

        :getter: Returns the domain
        :type: str
        """
        return self._domain

    @property
    def issuer(self) -> str:
        """The issuer configured for AWS Cognito

        :getter: Returns the issuer
        :type: str
        """
        return self._issuer

    @property
    def redirect_uri(self) -> str:
        """The redirect uri configured for AWS Cognito

        :getter: Returns the uri to redirect to after resolving auth code challenge
        :type: str
        """
        return self._redirect_uri

    @property
    def region(self) -> str:
        """The AWS region that contains the AWS Cognito instance

        :getter: Returns the AWS region
        :type: str
        """
        return self._region

    @property
    def userpool_id(self) -> str:
        """The id configured for AWS Cognito user pool.

        :getter: Returns the AWS Cognito user pool id
        :type: str
        """
        return self._userpool_id

    def __str__(self) -> str:
        """Return a string representation of properties contained in class instance

        :return: String containing the property values for class instance
        :rtype: str
        """
        return (
            "\nAWSCognito(\n\tauth_urls="
            + str(self._auth_urls)
            + ",\n\tclient_id="
            + self._client_id
            + ",\n\tclient_secret="
            + self._client_secret
            + ",\n\tredirect_uri="
            + self._redirect_uri
            + ",\n\tregion="
            + self._region
            + ",\n\tuserpool_id="
            + self._userpool_id
            + "\n)"
        )


class AWSCognitoAuthInfo(object):
    """
    Class with cognito base, discovery, jwks, logout and refresh url properties.

    :param base_url: The base url for the AWS Cognito user pool
    :type base_url: str
    :param discovery_url: The discovery url for the AWS Cognito user pool
    :type discovery_url: str
    :param jwks_url: The JWKS url for the AWS Cognito user pool
    :type jwks_url: str
    :param logout_url: The logout url for logging out client from all devices
    :type logout_url: str
    """

    def __init__(
        self, base_url: str, discovery_url: str, jwks_url: str, logout_url: str
    ) -> None:
        """Initialise base, discovery, jwks, logout and refresh urls."""

        self._base_url = base_url
        self._discovery_url = discovery_url
        self._jwks = None
        self._jwks_url = jwks_url
        self._logout_url = logout_url
        self._refresh_url = base_url + "/oauth2/token"

        logging.info("Discovery url is " + str(self._discovery_url))

    @property
    def base_url(self) -> str:
        """The base url for the AWS Cognito user pool

        :getter: Returns the base url
        :type: str
        """
        return self._base_url

    @property
    def discovery_url(self) -> str:
        """The discovery url for the AWS Cognito user pool

        :getter: Returns the discovery url
        :type: str
        """
        return self._discovery_url

    @property
    def jwks_url(self) -> str:
        """The JWKS url for the AWS Cognito user pool

        :getter: Returns the JWKS url
        :type: str
        """
        return self._jwks_url

    @property
    def logout_url(self) -> str:
        """The logout url for the AWS Cognito user pool

        :getter: Returns the logout url
        :type: str
        """
        return self._logout_url

    @property
    def refresh_url(self) -> str:
        """The refresh url for the AWS Cognito user pool

        :getter: Returns the refresh url
        :type: str
        """
        return self._refresh_url

    async def get_keyset(self, reload: bool = False):
        """Download the JWKS key set for the AWS Cognito user pool.

        On first download, the result is cached for subsequent access. The cache can be
        refreshed by setting the reload parameter to true.

        :param reload: Flag used to force the JWKS to be downloaded again
        :type reload: bool

        :raises: httpx.HTTPStatusError: on receipt of an exceptional response
        :raises: httpx.RequestError on transport, network, protocol fails etc.

        :return: The keyset used by the AWS Cognito user pool
        :rtype: :class:`authlib.jose.KeySet`
        """

        if self._jwks is not None and not reload:
            return self._jwks

        async with httpx.AsyncClient() as client:
            resp = await client.get(self.jwks_url)
            resp.raise_for_status()
            self._jwks = JsonWebKey.import_key_set(resp.text)

            return self._jwks

    def __str__(self) -> str:
        """Return a string representation for the AWS Cognito authorisation urls"""
        return (
            "\n\t\tAWSCognitoAuthInfo(\n\t\t\tbase_url="
            + self._base_url
            + ",\n\t\t\tdiscovery_url="
            + self._discovery_url
            + ",\n\t\t\tjwks="
            + str(self._jwks)
            + ",\n\t\t\tjwks_url="
            + self._jwks_url
            + ",\n\t\t\tlogout_url="
            + self._logout_url
            + ",\n\t\t\trefresh_url="
            + self._refresh_url
            + "\n\t\t)"
        )
