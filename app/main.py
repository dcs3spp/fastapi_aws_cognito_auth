import logging
from functools import partial
from os.path import dirname, join

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from starlette.middleware.sessions import SessionMiddleware

from . import cognito, context
from .auth import create_cognito_client
from .dependencies import get_settings, require_auth
from .doc import tags_metadata
from .routers import auth, core

logging.config.fileConfig("logging.conf", disable_existing_loggers=False)
logger = logging.getLogger(__name__)


def create_app() -> FastAPI:
    """Create and return a FastAPI application.

    Static files are served and mounted from the /static endpoint.
    Session middleware is initialised to enable oauth2 features provided by authlib.
    An application context is appended to the application state and made available on
    requests via a 'context' property, e.g.
    request.app.state.context: context.AppContext

    :return: FastAPI instance
    :rtype: :class:`fastapi.FastAPI`
    """

    static_dir = join(dirname(__file__), "static")

    app = FastAPI(
        tile="API Using AWS Cognito Authorization Code Flow",
        description=(
            "API implemented with FastAPI that uses AWS Cognito with authorization "
            "code flow to offer **login**, **logout**, **me** and **refresh** "
            "endpoints. Access and refresh tokens are stored within HTTP Only cookies."
        ),
        openapi_tags=tags_metadata,
    )
    app.add_middleware(SessionMiddleware, secret_key="some-random-string")
    app.mount("/static", StaticFiles(directory=static_dir), name="static")

    app.include_router(auth.router)
    app.include_router(core.router)

    app.add_event_handler(event_type="startup", func=partial(startup, app=app))

    return app


async def startup(app: FastAPI) -> None:
    """Initialise AppContext with cognito settings and oauth2 context. The oauth2
    context contains cognito token validator and authorisation for protected endpoints.

    :param app: FastAPI instance.
    :type app: :class:`fastapi.FastAPI`
    """

    ctx = context.AppContext()
    ctx.oauth2_settings = cognito.AWSCognito(get_settings())
    ctx.oauth2_context = await create_cognito_client(ctx.oauth2_settings, require_auth)

    app.state.context = ctx

    logger.debug("cognito config := " + str(app.state.context.oauth2_settings))
