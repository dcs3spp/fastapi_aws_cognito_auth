from functools import lru_cache
from os.path import dirname, join

from authlib.oauth2.rfc6749 import OAuth2Token, UnsupportedTokenTypeError
from fastapi import Cookie, HTTPException
from fastapi.requests import Request
from fastapi.templating import Jinja2Templates

from . import config
from .auth import CookieResourceProtector
from .context import AppContext, OAuth2Context

require_auth = CookieResourceProtector()


async def get_app_context(request: Request) -> AppContext:
    """Returns AppContext from request.app.state property.

    :param request: The underlying request made to the API
    :type request: :class:`fastapi.requests.Request`

    :raises AttributeError: When context property is unavailable

    :return: Application context
    :rtype: :class: `.context.AppContext`
    """
    if not hasattr(request.app.state, "context"):
        raise AttributeError("context attribute not set on app state")

    context: AppContext = request.app.state.context

    return context


@lru_cache()
def get_settings() -> config.Settings:
    """Returns config settings instance encapsulating app config. The settings are
    cached for later use.

    :return: Application config settings
    :rtype: :class:`config.Settings`
    """
    return config.Settings()


@lru_cache()
def get_templates(
    base_dir: str = join(dirname(__file__), "templates")
) -> Jinja2Templates:
    """Create, cache and return a jin2a templates instance.

    :param base_dir: The directory where templates are stored.
    :type base_dir: str

    :return: Jinja2 templates accessor
    :rtype: :class:`fastapi.templating.Jinja2Templates`
    """
    return Jinja2Templates(base_dir)


def get_access_token(
    session_token: str = Cookie(
        None,
        description="Cookie that stores access token",
    )
) -> str:
    """Retrieve value from session_token cookie.

    :param session_token: Access token stored in session_token cookie
    :type session_token: str

    :raises HTTPException: 400 if session_token cookie is not present
    :raises UnsupportedTokenTypeError: When session_token cookie value does not contain
        two parts, Bearer <token string>

    :return: Access token stored within the session_token cookie
    :rtype: str
    """
    if session_token is None:
        raise HTTPException(status_code=400, detail="Session token is missing")

    token_parts = session_token.split(None, 1)
    if len(token_parts) != 2:
        raise UnsupportedTokenTypeError()

    token_type, token = token_parts

    return token


def get_token_dict(
    request: Request,
    session_token: str = Cookie(
        None,
        description="Cookie that stores access token",
    ),
) -> OAuth2Token:
    """Retrieve authlib oauth2 access token representation

    :param request: The underlying API request
    :type request: :class:`Request`
    :param session_token: Access token stored in session_token cookie
    :type session_token: str

    :return: Access token represented by :class:`OAuth2Token` instance
    :rtype: :class:`OAuth2Token`
    """
    access_token: str = get_access_token(session_token)
    auth_context: OAuth2Context = request.app.state.context.oauth2_context

    return auth_context.token_validator.token_to_dict(access_token)


def get_refresh_token(
    refresh_token: str = Cookie(
        None,
        description="Cookie that stores refresh token",
    )
) -> str:
    """Retrieve value from refresh_token cookie.

    :param refresh_token: The value of refresh_token cookie
    :type refresh_token: str

    :raises: HTTPException: 400 bad request when refresh_token cookie not present

    :return: Value of refresh_token cookie
    :rtype: str
    """
    if refresh_token is None:
        raise HTTPException(status_code=400, detail="Refresh token is missing")

    return refresh_token
