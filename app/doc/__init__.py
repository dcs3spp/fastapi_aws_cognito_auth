"""Standard status responses and tags for OpenAPI documentation."""
from typing import Dict

_forbidden: str = (
    "Forbidden request, e.g. token decode error, invalid token signature, "
    "invalid expiry time value, expired token or refreshing an unexpired token."
)

auth_responses: Dict[int, Dict[str, str]] = {
    400: {"description": "Access token or refresh token cookie was not found"},
    401: {"description": str(_forbidden)},
}

tags_metadata = [
    {
        "name": "auth",
        "description": (
            "Operations for **login**, **logout**, **me** and "
            "**refreshing access tokens**"
        ),
    },
]

__all__ = [
    "auth_responses",
    "tags_metadata",
]
