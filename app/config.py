from pydantic import BaseSettings


class Settings(BaseSettings):
    """Settings initialised from .env file"""

    app_name: str = "Rapport API"

    client_id: str
    client_secret: str
    domain: str
    logout_redirect_uri: str
    redirect_uri: str
    region: str
    userpool_id: str

    class Config:
        env_file = ".env"
